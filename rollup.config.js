import fs from 'fs'

import dts from 'rollup-plugin-dts';
import babel from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';

const INPUT = './src/index.ts';
const LIBRARY_NAME = 'hermes';

const BASE_EXTERNAL_LIBRARIES = [
  'fp-ts/lib/Option',
  'fp-ts/lib/Either',
];
const externalLibraries = () => {
  const packageJSONPath = `${__dirname}/package.json`;
  const packageContent = JSON.parse(
    fs.readFileSync(packageJSONPath).toString());
  return [...BASE_EXTERNAL_LIBRARIES, ...Object.keys(packageContent.dependencies)];
};

export default [
  {
    input: INPUT,
    output: [{ file: `dist/${LIBRARY_NAME}.d.ts`, format: 'es' }],
    plugins: [dts()]
  },
  {
    input: INPUT,
    output: [
      {
        format: 'cjs',
        file: `./dist/${LIBRARY_NAME}.js`,
        exports: 'auto'
      },
      {
        format: 'es',
        file: `./dist/${LIBRARY_NAME}.es.js`,
        exports: 'auto'
      }
    ],
    external: externalLibraries(),
    plugins: [
      resolve({
        jsnext: true,
        extensions: ['.ts']
      }),
      commonjs(),
      babel({
        extensions: ['.ts'],
        include: ['src/**/*', 'src/*']
      })
    ]
  }
];
