import { expect } from 'chai';
import { describe, it } from 'mocha';

import { stringifyKeyValuePair } from './hermes.helpers';

describe('Hermes helpers module', () => {
  describe('stringifyKeyValuePair function', () => {
    it('should return a key value storage as a string', () => {
      const input = {
        hello: 'world',
        this: 1,
        is: 2,
        a: 'test',
        null: null
      };
      const expectedResult = 'hello = world;\nthis = 1;\nis = 2;\na = test;\nnull = null;';
      const result = stringifyKeyValuePair(input);
      expect(result).to.be.a('string');
      expect(result).to.eq(expectedResult);
    });
  });
});
