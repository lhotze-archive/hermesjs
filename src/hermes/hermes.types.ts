import {
  HttpRequest,
  HttpResponse,
  RecognizedString,
  TemplatedApp
} from 'uWebSockets.js';
import { Either } from 'fp-ts/lib/Either';
import { Option } from 'fp-ts/lib/Option';

export enum RequestMethods {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE'
}

/* -- Router Definitions -- */
export type RequestData = {
  readonly method: RequestMethods
  readonly route: string
  readonly queryParams: { readonly [key: string]: string }
  readonly body: any
  readonly headers: { readonly [key: string]: string }
}
export type RequestResponse = {
  readonly status: number
  readonly message?: string
  readonly responseType?: 'json' | 'text'
}
export type RouterFunction = (req: RequestData) => Promise<RequestResponse> | RequestResponse

/* -- Main Definitions -- */
export type HermesFunction = (router: RouterFunction, port?: number, app?: TemplatedApp) => void

export type RequestBody = {
  readonly [key: string]: string | number
}
export type Headers = {
  readonly [key: string]: string
}
export type QueryParams = {
  readonly [key: string]: string
}
export type HermesError = {
  readonly status?: RecognizedString
  readonly message?: string
  readonly error?: Error
}

export type RequestHandlerFunction = (
  router: RouterFunction,
  res: HttpResponse,
  req: HttpRequest
) => Promise<Either<HermesError, RequestResponse>>

/* -- Parsing Functions -- */
export type QueryParamsParserFunction = (req: HttpRequest) => QueryParams

export type MethodParserFunction = (req: HttpRequest) => Option<RequestMethods>

export type HeaderParserFunction = (req: HttpRequest) => Headers

export type BodyParserFunction = (res: HttpResponse) => Promise<Either<Error, RequestBody>>

export type GenHermesErrorFunction = (status: RecognizedString, message: string, error?: Error) => HermesError

export type ErrorHandlerFunction = (res: HttpResponse, error: HermesError) => void
