import { hermes } from './hermes/hermes.main';
import * as hermesTypes from './hermes/hermes.types';

export default hermes;

export type HermesFunction = hermesTypes.HermesFunction;
export type RouterFunction = hermesTypes.RouterFunction;

export type RequestBody = hermesTypes.RequestBody;
export type RequestData = hermesTypes.RequestData;
export type RequestMethods = hermesTypes.RequestMethods;
export type RequestResponse = hermesTypes.RequestResponse;
