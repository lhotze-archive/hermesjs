{
  description = "A minimalist web server based upon µWebSockets.js";
  
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-22.05;
    
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
  flake-utils.lib.eachSystem [ "x86_64-linux" "aarch64-linux" ] (system:
  let
    pkgs = import nixpkgs { inherit system; };
    
    shellHook = ''
      echo "Entering Hermes development shell"
    '';
   
    shellInputs = with pkgs; [
      git
      yarn
      nodePackages.typescript
    ];
    hermes-dev-shell = pkgs.mkShell {
      inherit shellHook;
      name = "hermes-dev-shell";
      buildInputs = shellInputs;
    };
  in
  { devShells.default = hermes-dev-shell; });
}